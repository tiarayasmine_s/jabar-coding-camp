// soal 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var str;
daftarHewan.forEach(function(){
  str = daftarHewan.sort();
});

console.log(str.join('\n'));

// soal 2
function introduce(){
    return "Nama Saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby;
}

var data = {name : "Tiara" , age : 24 , address : "Jalan GKGB" , hobby : "Reading"};
var perkenalan = introduce(data);
console.log(perkenalan);

//soal 3
function hitung_huruf_vokal(str){
  var huruf_vokal = 'aeiouAEIOU';
  var hitung = 0;
  
  for(var x = 0; x < str.length ; x++)
  {
    if (huruf_vokal.indexOf(str[x]) !== -1)
    {
      hitung += 1;
    }
  
  }
  return hitung;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)

//soal 4
function hitung(n){
    return n * 2 - 2;
}

console.log( hitung(0)); 
console.log( hitung(1));
console.log( hitung(2));
console.log( hitung(3));
console.log( hitung(5));